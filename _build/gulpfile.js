var gulp = require('gulp'),
	sass = require('gulp-sass'),
	browserSync = require('browser-sync').create();

gulp.task('hello', function() {
  // Stuff here
  console.log('When do we play?');
});

gulp.task('sass', function(){
  return gulp.src('../scss/*.scss')
    .pipe(sass()) // Using gulp-sass
    .pipe(gulp.dest('../css/'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: '../'
    },
  })
});

gulp.task('watch', ['browserSync'], function(){
  gulp.watch('../scss/**/*.scss', ['sass']); 
  // Other watchers
});


gulp.task('default', ['watch']);
